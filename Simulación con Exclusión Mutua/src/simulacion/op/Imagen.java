package simulacion.op;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Imagen 
{
	private BufferedImage img;
    private File file;
    private int width;
	private int height;
	private String dir;
	
	public Imagen(String direccion)
	{
		dir = direccion;
		img = null;
        file = null;
        
        try {
        	file = new File(direccion);
			img = ImageIO.read(file);
			width = img.getWidth();
	        height = img.getHeight();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String getDireccion() {
		return dir;
	}
	
	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
	
	public int getRGB(int x, int y) {
		return img.getRGB(x,y);
	}
}

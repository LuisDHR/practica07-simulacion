package simulacion.gui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.wb.swt.SWTResourceManager;

class ImageDialog extends Dialog 
{
	private String dir;
	
	public ImageDialog(Shell parent, String dir) {
		this(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL, dir);
	}

	public ImageDialog(Shell parent, int style, String dir) {
		// Let users override the default styles
	    super(parent, style);
	    this.dir = dir;
	}

	public void open() {
		Shell shell = new Shell(getParent(), getStyle());
	    shell.setText(getText());
	    createContents(shell);
	    shell.pack();
	    shell.open();
	    Display display = getParent().getDisplay();
	    while (!shell.isDisposed()) {
	      if (!display.readAndDispatch()) {
	        display.sleep();
	      }
	    }
	}

	private void createContents(final Shell shell) {
		//Create image
		Image image = new Image(shell.getDisplay(), dir);
		Image resize = null;
		
		// Show label
		Label lbl_Img = new Label(shell, SWT.NONE);
		lbl_Img.setBackground(SWTResourceManager.getColor(SWT.COLOR_LIST_FOREGROUND));
		lbl_Img.setBounds(1, 1, 600, 600);
		if(image.getBounds().height > 600 && image.getBounds().width > 800)
		{
			resize = MainWindow.resize(image);
			lbl_Img.setImage(resize);
		}
		else {
			lbl_Img.setImage(image);
		}
	}
}

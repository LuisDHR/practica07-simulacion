package simulacion.gui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.wb.swt.SWTResourceManager;

import simulacion.op.ImageOperator;
import simulacion.op.Imagen;

public class MainWindow {

	protected Shell shell;
	public static Display display;
	private Imagen img1;
    private Imagen img2;
    private boolean img1Selected = false;
    private boolean img2Selected = false;
	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			MainWindow window = new MainWindow();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		display = Display.getCurrent();
		shell = new Shell(display, SWT.CLOSE | SWT.TITLE | SWT.MIN);
		shell.setMinimumSize(new Point(1360, 768));
		shell.setMaximized(false);
		shell.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
		shell.setSize(1100, 650);
		shell.setText("Procesador de imagenes");
		
		FileDialog dialog = new FileDialog (shell, SWT.OPEN | SWT.MULTI);
		String[] filterNames = new String [] {"JPEG (*.jpg)", "PNG (*.png)"};
		String[] filterExtensions = new String [] {"*.jpg", "*.png"};
		String filterPath = "/home/luis/";
	    dialog.setFilterNames (filterNames);
	    dialog.setFilterExtensions (filterExtensions);
	    dialog.setFilterPath (filterPath);
		
	    FileDialog d = new FileDialog (shell, SWT.SAVE);
	    String[] filterN = new String [] {"JPEG (*.jpg)", "PNG (*.png)"};
	    String[] filterE = new String [] {"*.jpg", "*.png"};
	    String filterP = "/home/luis/";
	    d.setFilterNames (filterN);
	    d.setFilterExtensions (filterE);
	    d.setFilterPath (filterP);
	    
	    Label lbl_alpha = new Label(shell, SWT.NONE);
	    lbl_alpha.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
	    lbl_alpha.setVisible(false);
		lbl_alpha.setFont(SWTResourceManager.getFont("Roboto", 14, SWT.NORMAL));
		lbl_alpha.setBounds(550, 85, 220, 35);
		lbl_alpha.setText("Valor de alpha");
		
		Spinner alpha = new Spinner(shell, SWT.BORDER);
		alpha.setVisible(false);
		alpha.setIncrement(10);
		alpha.setDigits(2);
		alpha.setSelection(10);
		alpha.setFont(SWTResourceManager.getFont("Roboto", 14, SWT.NORMAL));
		alpha.setBounds(550, 125, 220, 45);
	    
	    CCombo comboOp = new CCombo(shell, SWT.BORDER | SWT.READ_ONLY);
	    comboOp.addSelectionListener(new SelectionAdapter() {
	    	@Override
	    	public void widgetSelected(SelectionEvent e) {
	    		if(comboOp.getSelectionIndex() == 2) {
	    			lbl_alpha.setVisible(true);
	    			alpha.setVisible(true);
	    		}
	    		else {
	    			lbl_alpha.setVisible(false);
	    			alpha.setVisible(false);
	    		}
	    	}
	    });
	    comboOp.setFont(SWTResourceManager.getFont("Roboto", 14, SWT.NORMAL));
		comboOp.setText("Operaci\u00f3n");
		comboOp.setVisibleItemCount(4);
		comboOp.add("Suma", 0);
		comboOp.add("Resta", 1);
		comboOp.add("Combinaci\u00f3n lineal", 2);
		comboOp.add("Multiplicaci\u00f3n", 3);
		comboOp.setBounds(550, 25, 220, 40);
		comboOp.select(0);
		
		Label lbl_cols = new Label(shell, SWT.NONE);
		lbl_cols.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
		lbl_cols.setFont(SWTResourceManager.getFont("Roboto", 14, SWT.NORMAL));
		lbl_cols.setBounds(550, 200, 220, 35);
		lbl_cols.setText("Columnas");
		
		Spinner cols = new Spinner(shell, SWT.BORDER);
		cols.setFont(SWTResourceManager.getFont("Arial", 14, SWT.NORMAL));
		cols.setMinimum(1);
		cols.setSelection(1);
		cols.setBounds(550, 240, 220, 45);
		
		Label lbl_rows = new Label(shell, SWT.NONE);
		lbl_rows.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
		lbl_rows.setFont(SWTResourceManager.getFont("Roboto", 14, SWT.NORMAL));
		lbl_rows.setBounds(550, 306, 220, 35);
		lbl_rows.setText("Renglones");
		
		Spinner rows = new Spinner(shell, SWT.BORDER);
		rows.setMinimum(1);
		rows.setSelection(1);
		rows.setFont(SWTResourceManager.getFont("Arial", 14, SWT.NORMAL));
		rows.setBounds(550, 345, 220, 45);
	    
		Label lbl_Img1 = new Label(shell, SWT.NONE);
		lbl_Img1.setBackground(SWTResourceManager.getColor(SWT.COLOR_LIST_FOREGROUND));
		lbl_Img1.setBounds(25, 25, 400, 400);
		
		Label lbl_Img2 = new Label(shell, SWT.NONE);
		lbl_Img2.setBackground(SWTResourceManager.getColor(SWT.COLOR_LIST_FOREGROUND));
		lbl_Img2.setBounds(900, 25, 400, 400);
		
		Button btnGenerar = new Button(shell, SWT.CENTER);
		btnGenerar.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		btnGenerar.setFont(SWTResourceManager.getFont("Roboto", 14, SWT.NORMAL));
		btnGenerar.setBackground(SWTResourceManager.getColor(SWT.COLOR_DARK_CYAN));
		btnGenerar.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				if(!img1Selected || !img2Selected)
				{
					MessageBox diag = new MessageBox(shell, SWT.ICON_WARNING | SWT.OK);
					diag.setMessage("Seleccione ambas im\u00e1genes para continuar.");
					diag.open();
					return;
				}
				
				int i = comboOp.getSelectionIndex();
				double a = 0.0;
				
				if(i == 2) {
	    			a = (alpha.getSelection() / Math.pow(10, alpha.getDigits())); 
	    		}
				
				d.open();
				String path = d.getFilterPath();
				String name = d.getFileName();
				String nombre = path + "/" + name;
				
				if(path.isEmpty() || name.isEmpty())
				{
					MessageBox diag = new MessageBox(shell, SWT.ICON_WARNING | SWT.OK);
					diag.setMessage("Imagen no seleccionada.");
					diag.open();
					return;
				}
				
				ImageOperator Op = new ImageOperator(img1, img2, cols.getSelection(), rows.getSelection());

				switch(i) {
					case 0:
						Op.setNombreOperacion(nombre, "suma");
						Op.processMatrix();
					break;
					
					case 1:
						Op.setNombreOperacion(nombre, "resta");
						Op.processMatrix();
					break;
					
					case 2:
						Op.setNombreOperacion(nombre, "combinacion", a);
						Op.processMatrix();
					break;
					
					case 3:
						Op.setNombreOperacion(nombre, "multiplicacion");
						Op.processMatrix();
					break;
				}
				
				ImageDialog dI = new ImageDialog(shell, nombre);
				dI.open();
			}
		});
		btnGenerar.setBounds(550, 593, 220, 60);
		btnGenerar.setText("Operar");
		
		Button btnImg1 = new Button(shell, SWT.NONE);
		btnImg1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				dialog.open();
				String path = dialog.getFilterPath();
				String name = dialog.getFileName();
				
				if(path.isEmpty() || name.isEmpty())
				{
					MessageBox diag = new MessageBox(shell, SWT.ICON_WARNING | SWT.OK);
					diag.setMessage("Imagen no seleccionada.");
					diag.open();
					return;
				}
				
				img1 = new Imagen(path + "/" + name);
				Image image = new Image(shell.getDisplay(), img1.getDireccion());
				Image resize = resize(image);
				
				if(img1.getHeight() < 300 || img1.getWidth() < 400)
				{
					MessageBox diag = new MessageBox(shell, SWT.ICON_WARNING | SWT.OK);
					diag.setMessage("Tama\u00f1o no soportado");
					diag.open();
					img1Selected = false;
				}
				else
				{
					lbl_Img1.setImage(resize);
					img1Selected = true;
				}
			}
		});
		btnImg1.setFont(SWTResourceManager.getFont("Arial", 12, SWT.NORMAL));
		btnImg1.setBounds(105, 445, 250, 50);
		btnImg1.setText("Cargar imagen 1");
		
		Button btnImg2 = new Button(shell, SWT.NONE);
		btnImg2.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				dialog.open();
				String path = dialog.getFilterPath();
				String name = dialog.getFileName();
				
				if(path.isEmpty() || name.isEmpty())
				{
					MessageBox diag = new MessageBox(shell, SWT.ICON_WARNING | SWT.OK);
					diag.setMessage("Imagen no seleccionada.");
					diag.open();
					return;
				}

				img2 = new Imagen(path + "/" + name);
				Image image = new Image(shell.getDisplay(), img2.getDireccion());
				Image resize = resize(image);
				
				if(img2.getHeight() < 300 || img2.getWidth() < 400)
				{
					MessageBox diag = new MessageBox(shell, SWT.ICON_WARNING | SWT.OK);
					diag.setMessage("Tama\u00f1o no soportado");
					diag.open();
					img2Selected = false;
				}
				else
				{
					lbl_Img2.setImage(resize);
					img2Selected = true;
				}
			}
		});
		btnImg2.setFont(SWTResourceManager.getFont("Arial", 12, SWT.NORMAL));
		btnImg2.setText("Cargar imagen 2");
		btnImg2.setBounds(975, 445, 250, 50);
		
	}
	
	public static Image resize(Image image) {
		// Resize to +- 400*300
		int w1 = image.getBounds().width;
		int h1 = image.getBounds().height;

		int w = image.getBounds().width;
		int h = image.getBounds().height;
		
		if(w1 > 400 && w1 > h1)
		{
			w = 400;
			h = (h1 * 400) / w1;
		}
		
		if(h1 > 400 && h1 > w1)
		{
			h = 400;
			w = (w1 * 400) / h1;
		}
		
		Image scaled = new Image(Display.getDefault(), w, h);
		GC gc = new GC(scaled);
		gc.setAntialias(SWT.ON);
		gc.setInterpolation(SWT.HIGH);
		gc.drawImage(image, 0, 0,image.getBounds().width, image.getBounds().height, 0, 0, w, h);
		gc.dispose();
		image.dispose();
		return scaled;
	}
}